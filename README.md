# Desafio Gifs Hakers

Aprender Python e Django com a Loeffa

![](header.png)


## Requisitos do desafio

Seu trabalho é criar um projeto em Django, que vai captar os melhores GIFs hacker da internet, e vai mostrá-los em uma página para votação, contendo a imagem GIF, o número de votos que ele recebeu até agora, e dois botões: vote up (+1) e vote down (-1).

* Hackers precisam ser organizados, então use o git (sugestão de server: gitlab) para versionamento;

* Tenha um arquivo README na raiz do projeto, explicando em detalhes como rodá-lo. Aproveite para explicar suas decisões de projeto aqui;

* Use a API REST de https://tenor.com/gifapi/documentation#quickstart-search para pegar os 20 primeiros GIFs com a palavra "hacker";

* Os hackers do mundo esperam ter acesso à apenas uma tela: a tela principal (index), com os GIFs "hacker" ordenados decrescente pelo número de votos, os botões de votação e o número de votos de cada GIF;

* Você não precisa implementar paginação: mostre todos os GIFs na mesma página. Também não precisa de autenticação: qualquer um pode votar quantas vezes quiser.

## Instalação

Instalar Python 3.8.6rc1 e Django 3.1.1 conforme link abaixo caso não tenha Python e Django instalado

https://docs.djangoproject.com/pt-br/3.1/howto/windows/

## Como usar

Executar os comandos abaixo para iniciar o servidor

```sh
pip install requests
python manage.py migrate
python manage.py runserver
```

abra o link [http://127.0.0.1:8000/](http://127.0.0.1:8000/) no seu navegador

## Histórico

* 1.0.1
	* Atualização do README
* 1.0.0
	* Sistema funcionando
* 0.1.0
	* Consulta básica dos gifs, models e template básico
* 0.0.1
    * Instalação do Django

## Decisões de Projeto

Eu nunca tinha instalado Python, nem Django e nunca tinha escrito uma linha de Python. Gostei da documentação e bibliotecas da linguagem. Estou empolgado para trabalhar com python.

* A primeira meia hora do desafio utilizei pare pesquisar e decidir qual SO, GIT server e IDE utilizar

* Escolhi o OS Windows 10 pois era o que eu tinha instalado, isso pouparia tempo de instalação do OS e foi fácil de [instalar Python e Django](https://docs.djangoproject.com/pt-br/3.1/howto/windows/)

* Escolhi [GITHUB](https://github.com) com servidor GIT pois é o GIT Server que conheço e tem integração com a IDE Atom

* Escolhi IDE [Atom](https://atom.io) pois é um editor que eu já utilizo e tem integração com [GITHUB](https://github.com). Fiquei bastante tentado a utilizar o PyCharm por indicações de vários vídeos do YouTube que assisti

* Segui grande parte do [tutorial do Django](https://docs.djangoproject.com/pt-br/3.1/intro/) e fui adaptando para o meu projeto

* Escolhi utilizar o SQLite pois já vem configurado com o Django. Sei que para sistemas maiores terei que configurar um banco de dados mais robusto

* Depois que o app já tinha algumas funcionalidade fui pesquisar um template para deixar o sistema mais apresentável

* A animação da ordenação inseri no final projeto. Tive alguns problemas com ela no IE11.

* Testei e ajustei o sistema para Google Chrome, IE Edge e IE11

## Meta

Helio Seichi Terashima – [@hesete](https://github.com/hesete)

[https://github.com/hesete/gifs-hacker](https://github.com/hesete/gifs-hacker)
