from django.db import models
from django.db.models import F


class Gifs(models.Model):
    gif_id = models.IntegerField(default=0, primary_key=True)
    gif_url = models.CharField(max_length=500)
    gif_votos = models.IntegerField(default=0)

    class Meta:
        ordering = [F('gif_votos').desc(nulls_last=True)]
