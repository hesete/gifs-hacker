from django.urls import path
from . import views

urlpatterns = [
    path('', views.gifs, name='index'),
    path('gifs', views.gifs, name='gifs'),
    path('votar', views.votar, name='votar'),
]
