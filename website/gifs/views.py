from django.shortcuts import render
from django.http import JsonResponse
from .models import Gifs
from django.core.serializers import serialize
from django.db.models import F

import requests


def gifs(request):
    response = requests.get('https://api.tenor.com/v1/search?media_filter=minimal&key=KRMA4M89W9C0&q=hacker&limit=20')
    data = response.json()
    gifsa = Gifs.objects.all()

    for v in data['results']:
        v['votos'] = 0
        try:
            v['votos'] = gifsa.filter(gif_id=v['id']).values_list('gif_votos', flat=True).first()
        except ValueError as e:
            print(e)

    return render(request, 'gifs/gifs.html', {
        'gifs': data['results']
    })


def votar(request):

    gtipo = request.POST.get('tipo')
    gid = request.POST.get('id')

    if gtipo == 'voteup':
        gif, _ = Gifs.objects.get_or_create(gif_id=gid)
        gif.gif_votos = F('gif_votos') + 1
        gif.save(update_fields=["gif_votos"])

    elif gtipo == "votedown":
        gif, _ = Gifs.objects.get_or_create(gif_id=gid)
        gif.gif_votos = F('gif_votos') - 1
        gif.save(update_fields=["gif_votos"])

    data = {'data': serialize('json', Gifs.objects.all())}
    return JsonResponse(data)
