var group = document.querySelector(".group");
var nodes = document.querySelectorAll(".view");
var total = nodes.length;
var ease = Power1.easeInOut;
var boxes = [];

for (var i = 0; i < total; i++) {

  var node = nodes[i];

  // Initialize transforms on node
  TweenLite.set(node, { x: 0});

  boxes[i] = {
    transform: node._gsTransform,
    x: node.offsetLeft,
    y: node.offsetTop,
    node:node };

}

function updateVotos(data){
  var dados = JSON.parse(data.data);
  dados.forEach(function(o,i){
    $("#id_"+o.pk).text(o.fields.gif_votos);
  });

  ordenar();
}
function voteup(id){
  $.ajax({
    type : "POST",
    cache : false,
    //contentType : "application/json",
    url : "/votar",
    data :  {id:id , tipo:"voteup" , csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val()} ,
    //dataType : 'json',
    success :updateVotos
  });
}

function votedown(id){
  $.ajax({
    type : "POST",
    cache : false,
    //contentType : "application/json",
    url : "/votar",
    data :  {id:id , tipo:"votedown", csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val()} ,
    //dataType : 'json',
    success :updateVotos
  });
}

function ordenar(){
  var ord = [];
  $(".view").addClass("ordenar");
  $(".votos").each(function(i,o){
    //console.log(i);
    //console.log($(o).text());
    ord.push({"id":$(o).attr("id"),"order":$(o).text()});
  });
  ord.sort(function(a, b){return b.order-a.order});
  ord.forEach(function(o,i){
    $("#"+o.id).parent("div").parent("div").css("order",i+1).removeClass("ordenar");
  });
  $(".ordenar").each(function(i,o){
    $(o).css("order",ordenar.length+1+i).removeClass("ordenar");
  });

  for (var i = 0; i < total; i++) {

    var box = boxes[i];

    var lastX = box.x;
    var lastY = box.y;

    box.x = box.node.offsetLeft;
    box.y = box.node.offsetTop;

    // Continue if box hasn't moved
    if (lastX === box.x && lastY === box.y) continue;

    // Reversed delta values taking into account current transforms
    var x = box.transform.x + lastX - box.x;
    var y = box.transform.y + lastY - box.y;

    // Tween to 0 to remove the transforms
    TweenLite.fromTo(box.node, 1.5, { x:x, y:y }, { x: 0, y: 0, ease:ease });
  }
}



ordenar();
